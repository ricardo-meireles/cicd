from django.db import models


class Task(models.Model):
    title = models.CharField(max_length=40)
    is_done = models.BooleanField(default=False)
    date = models.DateTimeField()

    def __str__(self):
        return self.title
