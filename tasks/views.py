from django.shortcuts import render

from .models import Task


def list_tasks(request):
    tasks = Task.objects.all()

    return render(request, 'tasks/tasks.html', {'tasks': tasks})
