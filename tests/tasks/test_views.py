import pytest


class TestListTasks:

    @pytest.mark.django_db
    def test_status_code(self, client):
        response = client.get('/tasks/')
        assert response.status_code == 200
